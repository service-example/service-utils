package com.example.service.utils.swagger;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.util.StringUtils;
import org.springframework.web.server.WebFilter;

import static org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication.Type.REACTIVE;
import static springfox.documentation.builders.BuilderDefaults.nullToEmpty;

@ConditionalOnWebApplication(type = REACTIVE)
@ConditionalOnProperty(
        value = "springfox.documentation.swagger-ui.enabled",
        havingValue = "true",
        matchIfMissing = true)
public class SwaggerConfig {
    @Value("${springfox.documentation.swagger-ui.base-url}")
    private String swaggerBaseUrl;

    @Bean
    public SwaggerUIConfigurer swaggerUiWebfluxConfigurer() {
        return new SwaggerUIConfigurer(fixup(swaggerBaseUrl));
    }

    @Bean
    public WebFilter uiForwarder() {
        return (exchange, chain) -> {
            String path = exchange.getRequest().getURI().getPath();
            if (path.matches(".*/swagger-ui/")) {
                String html = StringUtils.trimTrailingCharacter(path, '/') + "/index.html";
                return chain.filter(
                        exchange
                                .mutate()
                                .request(exchange.getRequest()
                                        .mutate()
                                        .path(html)
                                        .build())
                                .build());
            }
            return chain.filter(exchange);
        };
    }

    private String fixup(String swaggerBaseUrl) {
        return StringUtils.trimTrailingCharacter(nullToEmpty(swaggerBaseUrl), '/');
    }
}