package com.example.service.utils.kafka;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.StringDeserializer;

public abstract class JsonValueDeserializer<T> implements Deserializer<T> {
    private final ObjectMapper om = new ObjectMapper();

    private final StringDeserializer stringSerializer = new StringDeserializer();

    @Override
    public T deserialize(String topic, byte[] data) {
        try {
            return om.readValue(stringSerializer.deserialize(topic, data), getType());
        } catch (Exception e) {
            return null;
        }
    }

    protected abstract Class<T> getType();
}
