package com.example.service.utils.kafka;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.serialization.Serializer;
import org.apache.kafka.common.serialization.StringSerializer;

public abstract class JsonValueSerializer<T> implements Serializer<T> {
    private final ObjectMapper om = new ObjectMapper();

    private final StringSerializer stringSerializer = new StringSerializer();

    @Override
    public byte[] serialize(String topic, T data) {
        try {
            return stringSerializer.serialize(topic, om.writeValueAsString(data));
        } catch (Exception e) {
            return stringSerializer.serialize(topic, e.getMessage());
        }
    }

    protected abstract Class<T> getType();
}
