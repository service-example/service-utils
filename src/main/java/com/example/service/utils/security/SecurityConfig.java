package com.example.service.utils.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;

@Configuration
@EnableWebFluxSecurity
public class SecurityConfig {

    @Value("${springfox.documentation.swagger-ui.base-url}")
    private String swaggerBaseUrl = "/docs";

    private String[] getSwaggerPaths() {
        return new String[]{swaggerBaseUrl + "/**", "/v2/api-docs/**", "/v3/api-docs/**"};
    }

    @Bean
    SecurityWebFilterChain springSecurityFilterChain(ServerHttpSecurity http) {
        return http // @formatter:off
                .csrf()
                    .disable()
                .authorizeExchange()
                    .pathMatchers(getSwaggerPaths()).permitAll()
                    .and()
                .authorizeExchange()
                    .anyExchange().authenticated()
                    .and()
                .httpBasic()
                    .disable()
                .oauth2ResourceServer()
                    .jwt()
                        .and()
                .and()
                .build(); // @formatter:on
    }

}